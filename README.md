# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* 哭哭哭哭哭！！！我在我的電腦畫起來很正常，但是我push上去之後，從setting/pages點開的那個網址不能畫，但是架構都在。
* 如果助教也不能畫，請下載我上傳到ilms的那個，跟這個一模一樣，我真的不知道為什麼會這樣啊啊！
* 有跑pipeline/remove fork/設成public

* 第一步：畫布、普通筆、顏色、筆刷大小
* canvas設定長寬
畫筆找了網路上很多人家做過的小畫家，參考一種用法就是在畫布偵測到mousemove的時候紀錄經過的點然後stroke
顏色跟比刷大小也是參考過別人的
顏色基本有九種，按了就是那種顏色，在js抓誰被按了然後就更改strokestyle
另一種是用input range更改顏色，有rgb三個可以更改，數值會顯示（基本九色的數值不會顯示），也是在js抓三個value在運算傳給strokestyle
比刷大小固定6種，跟讀取顏色一樣，有點到就改變setLineWidth
一起做的還有橡皮擦功能，當初是想說直接把筆刷設成透明就可以，後來做了不同brush shape就變成按下橡皮擦，繪畫出透明了圖形，有點怪，所以改成按下橡皮擦會先改成一般筆刷，但是按回其他筆刷仍然可以用透明的
* 第二步：download、reset
* 下載比較簡單，標籤a有提供下載的功能，按下時把canvas當前狀態存成一個url就可以下載
reset就把畫布clearRect，然後記得要push()紀錄
* 第三步：上傳img、cursor icon
* 上傳功能是參考網路的資料，把上傳的東西onload成一個東西，然後把它畫在canvas上的概念
游標更改就是當點下筆刷或橡皮擦時，更改canvas.style.cursor
其他按鈕的游標寫在css裡面，游標移過去就會改成pointer或ew-resize的樣式
* 第四步：undo、redo
* 要做到這兩個功能就要記錄每個做完的時刻，所以當游標在canvas上mouseup時就記錄一次（一開始畫布就紀錄，上傳圖片也記錄，reset也會紀錄），方法是用一個array把canvas.toDataURL存起來（跟下載一樣），用一個count計數，undo/redo就把canvas清空然後再把array裡count±1的東西畫到canvas上
* 第五步：圓形、矩形、三角形
* 畫圖形跟普通筆刷不一樣，普通筆刷是mousemove的時候要一直畫，畫圖形式mousemove的時候一直預覽的感覺，直到mouseup才固定圖形，所以每次mousemove的時候就要一直清空畫布，在畫上一個狀態跟新的圖形
圓形用arc畫，矩形用strokeRect，三角形用lineTo，但是只控制兩個點所以畫出來是等腰三角形
畫圖行mousemove時會一直用到一下筆的那個點（圓心、四邊形左上角、三角形頂點），所以一mousedown就紀錄那個點
* 第六步：text input
* 用了很間單的方法，用input text（要輸入的字）+select（字體樣式）+input range（字體大小）
點text後，在輸入框打好字，選好字體跟大小，直接點畫布就會呈現出來，當然也可以選顏色（跟畫筆共用）
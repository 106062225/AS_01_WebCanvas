$(document).ready(function() {
  var canvas = $("#_canvas")[0];
  var context = canvas.getContext('2d');
  var drawing = false;
  var queue = [];
  var line = true;
  var circle = false;
  var rect = false;
  var tri = false;
  var txt = false;
  var beginx, beginy;
  $('#line')[0].addEventListener('click', function(){
    line = true;
    circle = false;
    rect = false;
    tri = false;
    txt = false;
    canvas.style.cursor = "url(mypen.png), auto";
  })
  $('#circle')[0].addEventListener('click', function(){
    line = false;
    circle = true;
    rect = false;
    tri = false;
    txt = false;
    canvas.style.cursor = "url(mypen.png), auto";
    
  })
  $('#rect')[0].addEventListener('click', function(){
    line = false;
    circle = false;
    rect = true;
    tri = false;
    txt = false;
    canvas.style.cursor = "url(mypen.png), auto";
  })
  $('#tri')[0].addEventListener('click', function(){
    line = false;
    circle = false;
    rect = false;
    tri = true;
    txt = false;
    canvas.style.cursor = "url(mypen.png), auto";
  })
  $('#text')[0].addEventListener('click', function(){
    line = false;
    circle = false;
    rect = false;
    tri = false;
    txt = true;
  })
  function change(){
    var pic = new Image();
    pic.src = array[count];
    context.drawImage(pic, 0, 0);
  }
  function shapeL(ctx,x,y,x1,y1) {
    ctx.beginPath();
    ctx.moveTo(x,y);
    ctx.lineTo(x1,y1);
    ctx.closePath();
    ctx.stroke();
  };
  function shapeC(ctx, x, y){
    ctx.clearRect(0,0,canvas.width,canvas.height);
    change();
    ctx.beginPath();
    ctx.arc(beginx, beginy, Math.sqrt(Math.pow(x-beginx, 2) + Math.pow(y-beginy, 2)), 0, Math.PI*2);
    ctx.stroke();
  }
  function shapeR(ctx,  x, y){
    ctx.clearRect(0,0,canvas.width,canvas.height);
    change();
    ctx.strokeRect(beginx, beginy, x-beginx, y-beginy);
  }
  function shapeT(ctx, x, y){
    ctx.clearRect(0,0,canvas.width,canvas.height);
    change();
    ctx.beginPath();
    ctx.moveTo(x,y);
    ctx.lineTo(beginx, beginy);
    ctx.lineTo(2*beginx-x, y);
    ctx.closePath();
    ctx.stroke();
  }
  function writetxt(x, y){
    var t = $('#textinput')[0].value;
    var fontsize = $('#textsize')[0].value;
    var fonttype = $('#select')[0].value;
    context.font = fontsize + "px " + fonttype;
    context.fillText(t, x, y);
    push();
  }
  $('#Reset')[0].addEventListener('click', function(){
    context.clearRect(0,0, canvas.width, canvas.height);
    push();
  })

  var save = $('#save')[0];
  save.addEventListener('click', function (e) {
    var dataURL = canvas.toDataURL();
    save.href = dataURL;
  });
  canvas.addEventListener('mousedown', function(e) {
    if(line) {
      drawing = true;
      var offset = $(e.currentTarget).offset()
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      shapeL(context,x,y,x,y);
      queue.push([x,y]);
    }
    else if(circle){
      drawing = true ;
      var offset = $(e.currentTarget).offset()
      beginx = e.pageX - offset.left;
      beginy = e.pageY - offset.top;
      queue.push([beginx, beginy]);
    }
    else if(rect){
      drawing = true ;
      var offset = $(e.currentTarget).offset()
      beginx = e.pageX - offset.left;
      beginy = e.pageY - offset.top;
      queue.push([beginx, beginy]);
    }
    else if(tri){
      drawing = true ;
      var offset = $(e.currentTarget).offset()
      beginx = e.pageX - offset.left;
      beginy = e.pageY - offset.top;
      queue.push([beginx, beginy]);
    }
    else if(txt){
      var offset = $(e.currentTarget).offset()
      beginx = e.pageX - offset.left;
      beginy = e.pageY - offset.top;
      queue.push([beginx, beginy]);
      writetxt(beginx, beginy);
    }
  });
  canvas.addEventListener('mouseup', function(e) {
    if(line && drawing) {
      push();
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      shapeL(context,old[0], old[1], x, y);
      drawing = false;
    }
    else if(circle && drawing){
      push();
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      drawing = false;
    }
    else if(rect && drawing){
      push();
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      drawing = false;
    }
    else if(tri && drawing){
      push();
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      drawing = false;
    }
    else if(txt){
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      drawing = false;
    }
  });
  canvas.addEventListener('mousemove', function(e) {
    if(line && drawing) {
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      shapeL(context,old[0],old[1],x,y);
      queue.push([x,y]);
    }
    else if(circle && drawing){
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      shapeC(context, x, y);
      queue.push([x,y]);
    }
    else if(rect && drawing){
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      shapeR(context, x, y);
      queue.push([x,y]);
    }
    else if(tri && drawing){
      var old = queue.shift();
      var offset = $(e.currentTarget).offset();
      var x = e.pageX - offset.left;
      var y = e.pageY - offset.top;
      shapeT(context, x, y);
      queue.push([x,y]);
    }
  });
  
  var color = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb'];
  var size = [1, 3, 5, 10, 15, 20];
  var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];
  
  function listener(i) {
    document.getElementById(color[i]).addEventListener('click', function() {
      context.strokeStyle = color[i];
      context.fillStyle = color[i];
      if(color[i]=="ebebeb"){
        canvas.style.cursor = "url(era.png), auto"
        line = true;
        circle = false;
        rect = false;
        tri = false;
        txt = false;
      }
    }, false);
  }
  
  function fontSizes(i) {
    document.getElementById(sizeNames[i]).addEventListener('click', function() {
      context.setLineWidth(size[i]);
    }, false);
  }


  for(var i = 0; i < color.length; i++) {
    listener(i);
  }
  
  for(var i = 0; i < size.length; i++) {
    fontSizes(i);
  }
  $('.xcolor input').change(function(){
    r = $('#xred').val();
    g = $('#xgreen').val();
    b = $('#xblue').val();
    changeColor(r,g,b);
  });

  function changeColor(r,g,b){
    colors = {
      xred : r,
      xgreen : g,
      xblue : b
    }
    $.each(colors, function(_color, _value) {
      $('#v'+_color).val(_value);
    });
    context.strokeStyle = "rgb("+r+","+g+","+b+")" ;
  };
 
  var imgupload = $('#upload')[0];
  imgupload.addEventListener('change' , paste, false);
  function paste(e){
      var loader = new FileReader();
      loader.onload = function(event){
          var img = new Image();
          img.onload = function(){
            context.drawImage(img, 0, 0);
          }
          img.src = event.target.result;
      }
      loader.readAsDataURL(e.target.files[0]);
  }

  var array = new Array();
  var count = -1;
  $('#undo')[0].addEventListener('click', undo);
  $('#redo')[0].addEventListener('click', redo);
  function push(){
      count++;
      if(count<array.length) array.length = count;
      array.push(canvas.toDataURL());
  }

  function undo(){
      if(count > 0){
          count--;
          fillarray();
      }
  }

  function redo(){
      if(count < array.length-1){
          count++;
          fillarray();
      }
  }
  function fillarray(){
    var pic = new Image();
    pic.src = array[count];
    pic.onload = function(){
      context.clearRect(0,0,canvas.width, canvas.height);
      context.drawImage(pic, 0, 0, canvas.width, canvas.height);
    } 
  }
  push();


});